module xiaomi

go 1.14

require (
	github.com/astaxie/beego v1.12.1
	github.com/dchest/captcha v0.0.0-20170622155422-6a29415a8364
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis v6.15.8+incompatible
	github.com/jinzhu/gorm v1.9.14
	github.com/onsi/ginkgo v1.13.0 // indirect
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	github.com/spf13/viper v1.7.0
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
)
