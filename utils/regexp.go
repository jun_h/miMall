package utils

import (
	"errors"
	"regexp"
)

const (
	EmailRe    = "^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+(\\.[a-zA-Z0-9_-])+"
	PhoneRe    = "^[1][3,4,5,7,8][0-9]{9}$"
	NameRe     = "^[\\w]{2,10}$"
	NicknameRe = "^[\u4e00-\u9fa5\\w]{2,6}$"
	PasswordRe = "^[\\w]{6,10}$"
)

func RegexpIsOk(re, data string) (bool, error) {
	return regexp.Match(re, []byte(data))
}

//用户名
func RegexpName(name string) error {
	if ok, _ := RegexpIsOk(NameRe, name);ok{
		return nil
	}
	return errors.New("用户名格式错误")
}

//邮箱
func RegexpEmail(email string) error {
	if ok, _ := RegexpIsOk(EmailRe, email);ok{
		return nil
	}
	return errors.New("邮箱格式错误")
}

//手机
func RegexpPhone(phone string) error {
	if ok, _ := RegexpIsOk(PhoneRe, phone);ok{
		return nil
	}
	return errors.New("手机格式错误")
}

//昵称
func RegexpNickname(nickname string) error {
	if ok, _ := RegexpIsOk(NicknameRe, nickname);ok{
		return nil
	}
	return errors.New("昵称格式错误")
}

//密码
func RegexpPassword(password string) error {
	if ok, _ := RegexpIsOk(PasswordRe, password);ok{
		return nil
	}
	return errors.New("密码格式错误")
}

//
func RegexpAll(name, email, phone, password string) error {

	if err := RegexpName(name); err != nil {
		return err
	} else if err := RegexpEmail(email); err != nil {
		return err
	} else if err := RegexpPhone(phone); err != nil {
		return err
	} else if err := RegexpPassword(password); err != nil {
		return err
	}

	return nil
}
