package utils

import (
	"math/rand"
	"time"
)
//随机字符串
func RandStringRunes(n int) string {
	var letterRunes = []rune("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

//contentByte := []rune(v.ArticleContent)
//var comment string
//if len(contentByte) >= 120 {
//	comment = string(contentByte[:110])
//} else {
//	comment = string(contentByte[:len(contentByte)-1])
//}