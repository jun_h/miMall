package utils

import "time"

//时间戳转时间
func UnixToDate(timestamp int) string {

	t := time.Unix(int64(timestamp), 0)

	return t.Format("2006-01-02 15:04:05")
}

//时间转时间戳
//2020-05-02 15:04:05
func DateToUnix(str string) int64 {
	template := "2006-01-02 15:04:05"
	t, err := time.ParseInLocation(template, str, time.Local)
	if err != nil {
		return 0
	}
	return t.Unix()
}

//获取当前时间戳(毫秒)
func GetUnix() int64 {
	return time.Now().Unix()
}
//获取当前时间戳(纳秒)
func GetUnixNano() int64 {
	return time.Now().UnixNano()
}
//获取当前时间  2006-01-02 15:04:05
func GetDate() string {
	template := "2006-01-02 15:04:05"
	return time.Now().Format(template)
}
//获取当前时间  20060102
func GetDay() string {
	template := "20060102"
	return time.Now().Format(template)
}