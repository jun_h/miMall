package middleware

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/url"
	"strings"
	"xiaomi/global"
	"xiaomi/global/response"
	"xiaomi/models"
)

// CurrentUser 获取登录用户
func CurrentUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		uid := session.Get("user_id")
		if uid != nil {
			user, err := models.GetUser(uid)
			if err == nil {
				c.Set("userInfo", &user)
			}
		}
		c.Next()
	}
}

// RequiredLoginAuth 需要登录
func RequiredLoginAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		if user, _ := c.Get("userInfo"); user != nil {
			if _, ok := user.(*models.Manager); ok {
				c.Next()
				return
			}
		}
		response.Error("请先登录", "/login", c)
		//c.JSON(200, )
		//c.Redirect(http.StatusMovedPermanently, "/login")
		c.Abort()
	}
}

//权限认证
func RequiredAuthorization() gin.HandlerFunc {
	return func(c *gin.Context) {
		user, _ := c.Get("userInfo")
		manager, _ := user.(*models.Manager)
		//判断是否是超级管理员
		if manager.IsSuper == 0 {
			//1获取到url
			pathname := c.Request.URL.Path
			pathname = strings.Replace(pathname, "/admin", "", 1)
			urlPath, _ := url.Parse(pathname)

			//2.管理员对应部门的所有权限
			var roleAccess []models.RoleAccess
			global.MysqlDb.Where("role_id=?", manager.RoleId).Find(&roleAccess)
			roleAccessMap := make(map[int]int)
			for _, v := range roleAccess {
				roleAccessMap[v.AccessId] = v.AccessId
			}
			//3、查出访问url信息
			var access models.Access
			global.MysqlDb.Where("url=?", urlPath.Path).Find(&access)

			//4、判断当前访问的url对应的权限id 是否在权限列表的id中
			if _, ok := roleAccessMap[access.Id]; !ok {
				response.Error("没有权限", "", c)
				c.Abort()
			}
		}
		c.Next()
		return
	}
}
