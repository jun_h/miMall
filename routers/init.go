package routers

import (

	"github.com/gin-gonic/gin"
	"xiaomi/global"
	"xiaomi/middleware"
)
func NewRouters() *gin.Engine {
	gin.DisableConsoleColor()
	app := gin.Default()
	// 模板中添加函数

	// 设置模板解析路径
	app.LoadHTMLGlob("./views/***/**/*")
	// 设置静态文件
	app.Static("/static", "./static")

	//设置session
	app.Use(middleware.Session(global.InitConfig.Session.SessionKey))
	//设置跨越
	//r.Use(middleware.Cors())
	//注册中间件
	app.Use(middleware.CurrentUser())

	Home(app)
	return app
}
