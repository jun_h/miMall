package routers

import (
	"github.com/gin-gonic/gin"
	"xiaomi/api"
)

func Home(r *gin.Engine) {
	r.GET("/", api.Api) //测试用的

	admin(r) //注册后台相关路由
}
