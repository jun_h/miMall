package routers

import (
	"github.com/gin-gonic/gin"
	"xiaomi/api"
	"xiaomi/middleware"
)

//后台的相关路由
func admin(r *gin.Engine) {
	admin := r.Group("/admin") //后台相关路由
	{
		admin.GET("/captcha", api.Captcha)       //验证码
		admin.GET("/login", api.AdminLogin)      //登录页面
		admin.POST("/doLogin", api.AdminDoLogin) //登录

		//下面是需要登录保护的
		loginAuth := admin.Group("")
		loginAuth.Use(middleware.RequiredLoginAuth()) //加载中间件
		{
			loginAuth.GET("/loginOut", api.AdminLoginOut)
			loginAuth.GET("", api.AdminIndex)
			loginAuth.GET("/welcome", api.AdminWelcome)
			loginAuth.GET("/main/changeStatus", api.ChangeStatus)
			loginAuth.GET("/main/editNum", api.EditNum)

			//下面是需要权限认证的的
			auth := loginAuth.Group("")
			auth.Use(middleware.RequiredAuthorization())
			{
				//管理员
				manager := auth.Group("/manager")
				{
					manager.GET("", api.AdminManager)
					manager.GET("/add", api.AdminManagerAdd)
					manager.POST("/doAdd", api.AdminDoManagerAdd)
					manager.GET("/edit", api.AdminManagerEdit)
					manager.POST("/doEdit", api.AdminDoManagerEdit)
					manager.GET("/delete", api.AdminManagerDelete)
				}

				//部门
				role := auth.Group("/role")
				{
					role.GET("", api.AdminRole)
					role.GET("/add", api.AdminRoleAdd)
					role.POST("/doAdd", api.AdminDoRoleAdd)
					role.GET("/edit", api.AdminRoleEdit)
					role.POST("/doEdit", api.AdminDoRoleEdit)
					role.GET("/delete", api.AdminRoleDelete)
					role.GET("/auth", api.AdminRoleAuth)
					role.POST("/doAuth", api.AdminDoRoleAuth)
				}

				//权限
				access := auth.Group("/access")
				{
					access.GET("", api.AdminAccess)
					access.GET("/add", api.AdminAccessAdd)
					access.POST("/doAdd", api.AdminDoAccessAdd)
					access.GET("/edit", api.AdminAccessEdit)
					access.POST("/doEdit", api.AdminDoAccessEdit)
					access.GET("/delete", api.AdminAccessDelete)
				}
				//轮播图
				focus := auth.Group("/focus")
				{
					focus.GET("", api.AdminFocus)
					focus.GET("/add", api.AdminFocusAdd)
					focus.POST("/doAdd", api.AdminDoFocusAdd)
					focus.GET("/edit", api.AdminFocusEdit)
					focus.POST("/doEdit", api.AdminDoFocusEdit)
					focus.GET("/delete", api.AdminFocusDelete)
				}
				//商品分类
				goodsCate := auth.Group("/goodsCate")
				{
					goodsCate.GET("", api.AdminGoodsCate)
					goodsCate.GET("/add", api.AdminGoodsCateAdd)
					goodsCate.POST("/doAdd", api.AdminDoGoodsCateAdd)
					goodsCate.GET("/edit", api.AdminGoodsCateEdit)
					goodsCate.POST("/doEdit", api.AdminDoGoodsCateEdit)
					goodsCate.GET("/delete", api.AdminGoodsCateDelete)
				}

				//商品类型
				goodsType := auth.Group("/goodsType")
				{
					goodsType.GET("", api.AdminGoodsType)
					goodsType.GET("/add", api.AdminGoodsTypeAdd)
					goodsType.POST("/doAdd", api.AdminDoGoodsTypeAdd)
					goodsType.GET("/edit", api.AdminGoodsTypeEdit)
					goodsType.POST("/doEdit", api.AdminDoGoodsTypeEdit)
					goodsType.GET("/delete", api.AdminGoodsTypeDelete)
				}

				//商品类型属性
				goodsTypeAttribute := auth.Group("/goodsTypeAttribute")
				{
					goodsTypeAttribute.GET("", api.AdminGoodsTypeAttr)
					goodsTypeAttribute.GET("/add", api.AdminGoodsTypeAttrAdd)
					goodsTypeAttribute.POST("/doAdd", api.AdminDoGoodsTypeAttrAdd)
					goodsTypeAttribute.GET("/edit", api.AdminGoodsTypeAttrEdit)
					goodsTypeAttribute.POST("/doEdit", api.AdminDoGoodsTypeAttrEdit)
					goodsTypeAttribute.GET("/delete", api.AdminGoodsTypeAttrDelete)
				}

				//商品类型属性
				goods := auth.Group("/goods")
				{
					goods.GET("", api.AdminGoods)
					goods.GET("/add", api.AdminGoodsAdd)
					goods.POST("/doAdd", api.AdminDoGoodsAdd)
					goods.GET("/edit", api.AdminGoodsEdit)
					goods.POST("/doEdit", api.AdminDoGoodsEdit)
					goods.GET("/delete", api.AdminGoodsDelete)

					goods.POST("/doUpload", api.DoUpload)
					goods.GET("/getGoodsTypeAttribute", api.GetGoodsTypeAttribute)
					goods.GET("/changeGoodsImageColor", api.ChangeGoodsImageColor)
					goods.GET("/removeGoodsImage", api.RemoveGoodsImage)
				}

			}

		}

	}
}
