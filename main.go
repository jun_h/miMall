package main

import (
	"xiaomi/initialize"
	"xiaomi/routers"
)

func main() {

	initialize.Init()

	// 监听端口，默认在8080
	app := routers.NewRouters()
	app.Run(":1206")
	//defer models.CloseDb()
}