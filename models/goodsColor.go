package models



//商品的颜色
type GoodsColor struct {
	Id         int    //id
	ColorName  string //颜色标题
	ColorValue string //颜色值
	Status     int    //状态
	Checked    bool   `gorm:"-"` //用于判断是否选中
}

func (GoodsColor) TableName() string {
	return "goods_color"
}
