package request

type AdminFocusRequest struct {
	Id        int    `form:"id"`
	Title     string `form:"title"  binding:"required"` //标题
	FocusType int    `form:"focus_type"  binding:"required"` //类型：1.网站，2.app,3.小程序
	Link      string `form:"link"  binding:"required"` //跳转地址
	Sort      int    `form:"sort"  binding:"required"` //排序
	Status    int    `form:"status"  `                   //是否显示
}
