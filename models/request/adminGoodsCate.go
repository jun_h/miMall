package request

type AdminGoodsCateRequest struct {
	Id          int    `form:"id"`
	Title       string `form:"title"  binding:"required"`
	Link        string `form:"link"`
	Template    string `form:"template"`
	Pid         int    `form:"pid"`
	SubTitle    string `form:"sub_title"`
	Keywords    string `form:"keywords"`
	Description string `form:"description"`
	Sort        int    `form:"sort"`
	Status      int    `form:"status"`
}
