package request

//添加管理员
type AdminManagerRequest struct {
	Id int    `form:"id"`
	Username string `form:"username"`
	Password string `form:"password"  binding:"required"`
	Mobile   string `form:"mobile"  binding:"required"`
	Email    string `form:"email"  binding:"required"`
	RoleId   int    `form:"role_id"  binding:"required"`
}
