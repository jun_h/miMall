package request

type AdminLoginRequest struct {
	Username string `form:"username"  binding:"required"`
	Password string `form:"password"  binding:"required"`
	Captcha  string `form:"captcha"  binding:"required"`
}
