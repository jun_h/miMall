package request

type AdminRoleRequest struct {
	Id          int    `form:"id"`
	Title       string `form:"title"  binding:"required"`
	Description string `form:"description"  binding:"required"`
}
