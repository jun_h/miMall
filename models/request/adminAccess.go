package request

//添加管理员
type AdminAccessRequest struct {
	Id          int    `form:"id"`
	ModuleName  string `form:"module_name"  binding:"required"`
	ActionName  string `form:"action_name"  binding:"required"`
	Type        int    `form:"type"  binding:"required"`
	Url         string `form:"url" `
	ModuleId    int    `form:"module_id"`
	Sort        int    `form:"sort"  binding:"required"`
	Description string `form:"description"`
	Status      int   `form:"status" `
}
