package request

type AdminGoodsRequest struct {
	Id             int      `form:"id" `
	Title          string   `form:"title"`
	SubTitle       string   `form:"sub_title"`
	GoodsSn        string   `form:"goods_sn"`
	CateId         int      `form:"cate_id"`
	GoodsNumber    int      `form:"goods_number"`
	Price          float64  `form:"price"`
	MarketPrice    float64  `form:"market_price"`
	RelationGoods  string   `form:"relation_goods"`
	GoodsAttr      string   `form:"goods_attr"`
	GoodsVersion   string   `form:"goods_version"`
	GoodsGift      string   `form:"goods_gift"`
	GoodsFitting   string   `form:"goods_fitting"`
	GoodsColor     []string `form:"goods_color"`
	GoodsKeywords  string   `form:"goods_keywords"`
	GoodsDesc      string   `form:"goods_desc"`
	GoodsContent   string   `form:"goods_content"`
	IsDelete       int      `form:"is_delete"`
	IsHot          int      `form:"is_hot"`
	IsBest         int      `form:"is_best"`
	IsNew          int      `form:"is_new"`
	GoodsTypeId    int      `form:"goods_type_id"`
	Sort           int      `form:"sort"`
	Status         int      `form:"status"`
	GoodsImageList []string `form:"goods_image_list"`
	AttrIdList     []string `form:"attr_id_list"`
	AttrValueList  []string `form:"attr_value_list"`
}
