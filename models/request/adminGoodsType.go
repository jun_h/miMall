package request

type AdminGoodsTypeRequest struct {
	Id          int    `form:"id"`
	Title       string `form:"title"  binding:"required"`
	Description string `form:"description"`
	Status      int   `form:"status"`
}
