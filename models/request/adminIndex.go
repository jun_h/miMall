package request

//公共的方法
type AdminIndexRequest struct {
	Id    int `form:"id"  binding:"required"`	//id
	Table string `form:"table"  binding:"required"`//表名
	Field string `form:"field"  binding:"required"`//字段名
	Num   int    `form:"num"`//数量
}
