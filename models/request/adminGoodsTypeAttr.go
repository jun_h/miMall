package request

type AdminGoodsTypeAttrRequest struct {
	Id        int    `form:"id"`
	CateId    int    `form:"cate_id"  binding:"required"`
	AttrType  int    `form:"attr_type"  binding:"required"`
	Title     string `form:"title"  binding:"required"`
	AttrValue string `form:"attr_value" `
	Sort      int    `form:"sort"`
}
