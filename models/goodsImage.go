package models



//商品的相册
type GoodsImage struct {
	Id      int//id
	GoodsId int//商品id
	ImgUrl  string//跳转url
	ColorId int//图片对应的颜色id
	Sort    int//排序
	Status  int//状态
	AddTime int64//增加时间
}

func (GoodsImage) TableName() string {
	return "goods_image"
}
