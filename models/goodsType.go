package models


//商品类型模型
type GoodsType struct {
	Id          int    //id
	Title       string //类型标题
	Description string //类型描述
	Status      int    //类型状态
	AddTime     int64    //类型增加时间
}

func (GoodsType) TableName() string {
	return "goods_type"
}
