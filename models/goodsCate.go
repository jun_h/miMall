package models


//商品分类模型
type GoodsCate struct {
	Id            int         //id
	Title         string      //标题
	CateImg       string      //分类图片
	Link          string      //跳转地址
	Template      string      //分类模板
	Pid           int         //自关联，0：顶级分类，其他：和id相关联，子节点
	SubTitle      string      //seo标题
	Keywords      string      //soe优化相关
	Description   string      //soe优化相关
	Sort          int         //排序
	Status        int         //状态
	AddTime       int64         //增加时间
	GoodsCateItem []GoodsCate `gorm:"foreignkey:Pid;association_foreignkey:Id"` //自关联
}

func (GoodsCate) TableName() string {
	return "goods_cate"
}
