package models



//商品的规格包装
type GoodsTypeAttr struct {
	Id              int    //id
	GoodsId         int    //商品id
	AttributeCateId int    //属性分类id
	AttributeId     int    //属性id
	AttributeTitle  string //属性标题
	AttributeType   int    //属性分类
	AttributeValue  string //属性值
	Sort            int    //排序
	AddTime         int64    //增加时间
	Status          int    //状态
}

func (GoodsTypeAttr) TableName() string {
	return "goods_type_attr"
}
