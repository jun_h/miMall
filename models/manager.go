package models

import (
	"golang.org/x/crypto/bcrypt"
	"xiaomi/global"
)

//管理员模型
type Manager struct {
	Id       int    //id
	Username string //用户名
	Password string //用户密码
	Mobile   string //手机号
	Email    string //邮箱
	Status   int   //状态
	RoleId   int    //部门
	AddTime  int64  //添加时间
	IsSuper  int    //是否是超级管理员
	Role     Role   `gorm:"foreignkey:Id;association_foreignkey:RoleId"`
}

const (
	// PassWordCost 密码加密难度
	PassWordCost = 12
	// Active 激活用户
	Active int = 1
	// Inactive 未激活用户
	Inactive uint = 0
	// Suspend 被封禁用户
	Suspend uint = 2
)

func (manager *Manager) TableName() string {
	return "manager"
}

// GetUser 用ID获取用户
func GetUser(ID interface{}) (Manager, error) {
	var user Manager
	result := global.MysqlDb.First(&user, ID)
	return user, result.Error
}

// SetPassword 设置密码
func (manager *Manager) SetPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), PassWordCost)
	if err != nil {
		return err
	}
	manager.Password = string(bytes)
	return nil
}

// CheckPassword 校验密码
func (manager *Manager) CheckPassword(password string) error {

	return bcrypt.CompareHashAndPassword([]byte(manager.Password), []byte(password))
}
