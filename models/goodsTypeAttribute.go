package models



//动态生成商品属性模型（包装规格里面显示的）
type GoodsTypeAttribute struct {
	Id        int    `json:"id"`         //id
	CateId    int    `json:"cate_id"`    //属性类型id
	Title     string `json:"title"`      //标题
	AttrType  int    `json:"attr_type"`  //类型（用于后台动态生成单行框，多行框，下拉框）
	AttrValue string `json:"attr_value"` //值
	Status    int    `json:"status"`     //状态
	Sort      int    `json:"sort"`       //排序
	AddTime   int64    `json:"add_time"`   //增加时间
}

func (GoodsTypeAttribute) TableName() string {
	return "goods_type_attribute"
}
