package models

//权限模型
type Access struct {
	Id          int
	ModuleName  string   //模块名称
	ActionName  string   //操作名称
	Type        int      //节点类型 :  1、表示模块    2、表示菜单     3、操作
	Url         string   //路由跳转地址
	ModuleId    int      //此module_id和当前模型的_id关联      module_id= 0 表示模块，其他：表示id下的子节点
	Sort        int      //排序
	Description string   //描述
	Status      int     //状态
	AddTime     int64    //添加时间
	AccessItem  []Access `gorm:"foreignkey:ModuleId;association_foreignkey:Id"` //自关联
	Checked     bool     `gorm:"-"`                                             // 数据库忽略本字段，用于判断此权限是否选中
}

func (Access) TableName() string {
	return "access"
}
