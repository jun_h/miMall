package models

//部门模型-----
type Role struct {
	Id          int//id
	Title       string//标题
	Description string //描述
	Status      int   //状态
	AddTime     int64  //创建时间
}

func (Role) TableName() string {
	return "role"
}
