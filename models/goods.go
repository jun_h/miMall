package models


//商品模型
type Goods struct {
	Id            int     //id
	Title         string  //标题
	SubTitle      string  //优惠标题
	GoodsSn       string  //商品的sn号
	CateId        int     //商品的分类
	ClickCount    int     //商品的点击数量
	GoodsNumber   int     //商品的库存
	Price         float64 //商品的价格
	MarketPrice   float64 //商品的原价
	RelationGoods string  //关联的商品
	GoodsAttr     string  //商品的属性
	GoodsVersion  string  //商品的版本
	GoodsImg      string  //商品图片
	GoodsGift     string  //商品关联的赠品
	GoodsFitting  string  //商品关联的配件
	GoodsColor    string  //商品颜色
	GoodsKeywords string  //seo优化相关
	GoodsDesc     string  //seo优化相关
	GoodsContent  string  //商品的详情
	IsDelete      int     //软删除
	IsHot         int     //是否热销
	IsBest        int     //是否推荐
	IsNew         int     //是否新品
	GoodsTypeId   int     //商品关联的属性类型。（包装规格）
	Sort          int     //排序
	Status        int     //商品状态
	AddTime       int64     //商品增加时间
}

func (Goods) TableName() string {
	return "goods"
}
