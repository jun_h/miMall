package models

//中间表(关联部门对应的权限)
type RoleAccess struct {
	AccessId int//权限
	RoleId   int//部门
}

func (RoleAccess) TableName() string {
	return "role_access"
}
