package models

//轮播图模型
type Focus struct {
	Id        int    //id
	Title     string //标题
	FocusType int    //类型：1.网站，2.app,3.小程序
	FocusImg  string //图片地址
	Link      string //跳转地址
	Sort      int    //排序
	Status    int    //是否显示
	AddTime   int64    //时间
}

func (*Focus) TableName() string {
	return "focus"
}
