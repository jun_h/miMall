package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//验证码
func Captcha(c *gin.Context) {
	admin.Captcha(c)
}

//登录页面
func AdminLogin(c *gin.Context) {
	c.HTML(http.StatusOK, "admin_login.html", gin.H{})
}
//登录
func AdminDoLogin(c *gin.Context) {
	var login request.AdminLoginRequest
	if err := c.ShouldBind(&login);err==nil{
		if err := admin.DoLogin(c,login);err==nil {
			response.Success("登录成功","",c)
		}else {
			response.Error(err.Error(),"/login",c)
		}
	}else {
		response.Error("非法的请求","/login",c)
	}
}

//退出登录
func AdminLoginOut(c *gin.Context) {
	if err := admin.LoginOut(c);err!=nil{
		//这里有日志
	}
	response.Success("退出登录成功","/login",c)
}
