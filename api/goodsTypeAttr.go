package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//类别列表
func AdminGoodsTypeAttr(c *gin.Context) {
	if cateId := c.Query("cate_id"); cateId != "" {
		if goodsType, goodsTypeAttrList, err := admin.GoodsTypeAttr(cateId); err == nil {
			c.HTML(http.StatusOK, "admin_goodsTypeAttr_index.html", gin.H{
				"goodsType":         goodsType,
				"goodsTypeAttrList": goodsTypeAttrList,
			})
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}

}

//增加类别属性
func AdminGoodsTypeAttrAdd(c *gin.Context) {
	cateStrId := c.Query("cate_id")
	if cateId, err := strconv.Atoi(cateStrId); err == nil {
		if goodsTypeList, err := admin.GoodsTypeAttrAdd(cateId); err == nil {
			c.HTML(http.StatusOK, "admin_goodsTypeAttr_add.html", gin.H{
				"goodsTypeList": goodsTypeList,
				"cateId":        cateId,
			})
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}

}

//增加部门操作
func AdminDoGoodsTypeAttrAdd(c *gin.Context) {
	var rGoodsTypeAttr request.AdminGoodsTypeAttrRequest
	if err := c.ShouldBind(&rGoodsTypeAttr); err == nil {
		if err := admin.DoGoodsTypeAttrAdd(rGoodsTypeAttr); err == nil {
			response.Success("添加成功", "/goodsTypeAttribute?cate_id="+strconv.Itoa(rGoodsTypeAttr.CateId), c)
		} else {
			response.Error("添加失败"+err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}

}

//编辑部门
func AdminGoodsTypeAttrEdit(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if goodsTypeList, goodsTypeAttr, err := admin.GoodsTypeAttrEdie(id); err == nil {
			c.HTML(http.StatusOK, "admin_goodsTypeAttr_edit.html", gin.H{
				"goodsTypeAttr": goodsTypeAttr,
				"goodsTypeList": goodsTypeList,
			})
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}
}

//编辑部门操作
func AdminDoGoodsTypeAttrEdit(c *gin.Context) {
	var rGoodsTypeAttr request.AdminGoodsTypeAttrRequest
	if err := c.ShouldBind(&rGoodsTypeAttr); err == nil {
		if err := admin.DoGoodsTypeAttrEdie(rGoodsTypeAttr); err == nil {
			response.Success("修改信息成功", "/goodsTypeAttribute?cate_id="+strconv.Itoa(rGoodsTypeAttr.CateId), c)
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}

}

//删除部门操作
func AdminGoodsTypeAttrDelete(c *gin.Context) {
	cateId := c.Query("cate_id")
	id := c.Query("id")
	if id != "" && cateId != "" {
		if err := admin.GoodsTypeAttrDelete(id); err == nil {
			response.Success("删除成功", c.Request.Referer(), c)
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}
}
