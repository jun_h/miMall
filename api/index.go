package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xiaomi/global/response"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

func AdminIndex(c *gin.Context) {

	userInfo, _ := c.Get("userInfo") //从session中拿到登录的用户

	if manager, ok := userInfo.(*models.Manager); ok {
		if indexInfo, err := admin.Index(manager); err == nil {
			c.HTML(http.StatusOK, "admin_main_index.html", indexInfo)
		} else {
			response.Error(err.Error(), "/login", c)
		}
	} else {
		response.Error("请先登录", "/login", c)
	}
}

func AdminWelcome(c *gin.Context) {
	c.HTML(http.StatusOK, "admin_main_welcome.html", gin.H{})
}

//修改公共状态
func ChangeStatus(c *gin.Context) {
	var index request.AdminIndexRequest

	m := map[string]interface{}{
		"success": false,
		"msg":     "更新数据失败",
	}
	var err error

	if err = c.ShouldBind(&index); err == nil {
		if err = admin.ChangeStatus(index); err == nil {
			m["success"] = true
			m["msg"] = "更新数据成功"
		}
	}

	c.JSON(http.StatusOK, m)
}

//修改数量的公共方法
func EditNum(c *gin.Context) {
	var index request.AdminIndexRequest

	m := map[string]interface{}{
		"success": false,
		"msg":     "更新数据失败",
	}
	var err error
	if err = c.ShouldBind(&index); err == nil {
		if err = admin.EditNum(index); err == nil {
			m["success"] = true
			m["msg"] = "更新数据成功"
		}
	}
	c.JSON(http.StatusOK, m)

}
