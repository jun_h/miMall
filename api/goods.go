package api

import (
	"github.com/gin-gonic/gin"
	"html/template"
	"math"
	"net/http"
	"strconv"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//列表
func AdminGoods(c *gin.Context) {
	//当前页
	page := c.Query("page")
	pageId, _ := strconv.Atoi(page)
	if pageId == 0 {
		pageId = 1
	}
	//每一页显示的数量
	pageSize := 3
	//实现搜索功能
	keyword := c.Query("keyword")

	if goodsList, count, err := admin.Goods(pageId, pageSize, keyword); err == nil {
		if len(goodsList) == 0 && page != "0" {
			prvPage := pageId - 1
			if prvPage == 0 {
				prvPage = 1
			}
			c.Redirect(http.StatusFound, "/goods?page="+strconv.Itoa(prvPage))
		} else {
			c.HTML(http.StatusOK, "admin_goods_index.html", gin.H{
				"goodsList":  goodsList,
				"totalPages": math.Ceil(float64(count) / float64(pageSize)),
				"page":       pageId,
				"keyword":    keyword,
			})
		}

	} else {
		response.Error(err.Error(), "", c)
	}

}

//增加
func AdminGoodsAdd(c *gin.Context) {
	if colors, cates, types, err := admin.GoodsAdd(); err == nil {
		c.HTML(http.StatusOK, "admin_goods_add.html", gin.H{
			"goodsCateList": cates,
			"goodsColor":    colors,
			"goodsType":     types,
		})
	} else {
		response.Error("加载页面失败"+err.Error(), c.Request.Referer(), c)
	}

}

//增加操作
func AdminDoGoodsAdd(c *gin.Context) {
	var rGoods request.AdminGoodsRequest
	if err := c.ShouldBind(&rGoods); err == nil {
		if err := admin.DoGoodsAdd(rGoods, c); err == nil {
			response.Success("添加成功", "/goods", c)
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}

}

//编辑
func AdminGoodsEdit(c *gin.Context) {

	if id := c.Query("id"); id != "" {

		if goods, cates, colors, images, types, goodsAttrStr, err := admin.GoodsEdie(id); err == nil {
			c.HTML(http.StatusOK, "admin_goods_edit.html", gin.H{
				"goods":         goods,
				"goodsCateList": cates,
				"goodsColor":    colors,
				"goodsImage":    images,
				"goodsType":     types,
				"goodsAttrStr":  template.HTML(goodsAttrStr),
				"prevPage":      c.Request.Referer(),
			})
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}
}

//编辑操作
func AdminDoGoodsEdit(c *gin.Context) {
	var rGoods request.AdminGoodsRequest
	prevPage := c.PostForm("prevPage")
	if err := c.ShouldBind(&rGoods); err == nil {
		if err := admin.DoGoodsEdie(rGoods, c); err == nil {
			response.Success("修改信息成功", prevPage, c)
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}

}

//删除操作
func AdminGoodsDelete(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if err := admin.GoodsDelete(id); err == nil {
			response.Success("删除成功", c.Request.Referer(), c)
		} else {
			response.Error(err.Error(), c.Request.Referer(), c)
		}
	} else {
		response.Error("非法的请求", c.Request.Referer(), c)
	}
}

func DoUpload(c *gin.Context) {
	m := admin.DoUpload(c)
	c.JSON(http.StatusOK, m)
}

//获取商品类型属性
func GetGoodsTypeAttribute(c *gin.Context) {
	cateId := c.Query("cate_id")
	m := admin.GetGoodsTypeAttribute(cateId)
	c.JSON(http.StatusOK, m)
}

//修改图片对应颜色信息
func ChangeGoodsImageColor(c *gin.Context) {
	colorId := c.Query("color_id")
	goodsImageId := c.Query("goods_image_id")
	m := admin.ChangeGoodsImageColor(colorId, goodsImageId)
	c.JSON(http.StatusOK, m)
}

//删除图库
func RemoveGoodsImage(c *gin.Context) {
	goodsImageId := c.Query("goods_image_id")
	m := admin.RemoveGoodsImage(goodsImageId)
	c.JSON(http.StatusOK, m)
}
