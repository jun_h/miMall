package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//部门列表
func AdminRole(c *gin.Context) {
	if roleList, err := admin.Role(); err == nil {
		c.HTML(http.StatusOK, "admin_role_index.html", roleList)
	} else {
		response.Error(err.Error(), "", c)
	}

}

//增加部门
func AdminRoleAdd(c *gin.Context) {
	c.HTML(http.StatusOK, "admin_role_add.html", gin.H{})
}

//增加部门操作
func AdminDoRoleAdd(c *gin.Context) {
	var rRole request.AdminRoleRequest
	if err := c.ShouldBind(&rRole); err == nil {
		if err := admin.DoRoleAdd(rRole); err == nil {
			response.Success("添加部门成功", "/role", c)
		} else {
			response.Error(err.Error(), "/role/add", c)
		}
	} else {
		response.Error("非法的请求", "/role/add", c)
	}

}

//编辑部门
func AdminRoleEdit(c *gin.Context) {

	if id := c.Query("id"); id != "" {
		if role, err := admin.RoleEdie(id); err == nil {
			c.HTML(http.StatusOK, "admin_role_edit.html", role)
		} else {
			response.Error(err.Error(), "/role", c)
		}
	} else {
		response.Error("非法的请求", "/role", c)
	}
}

//编辑部门操作
func AdminDoRoleEdit(c *gin.Context) {
	var rRole request.AdminRoleRequest
	if err := c.ShouldBind(&rRole); err == nil {
		if err := admin.DoRoleEdie(rRole); err == nil {
			response.Success("修改管理员信息成功", "/role", c)
		} else {
			response.Error(err.Error(), "/role/add", c)
		}
	} else {
		response.Error("非法的请求", "/role/add", c)
	}

}

//删除部门操作
func AdminRoleDelete(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if err := admin.RoleDelete(id); err == nil {
			response.Success("删除管理员成功", "/role", c)
		} else {
			response.Error(err.Error(), "/role", c)
		}
	} else {
		response.Error("非法的请求", "/role", c)
	}
}

//授权
func AdminRoleAuth(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if accesses, err := admin.RoleAuth(id); err == nil {
			c.HTML(http.StatusOK, "admin_role_auth.html", gin.H{
				"accessList":accesses,
				"roleId":id,
			})
		} else {
			response.Error(err.Error(), "/role", c)
		}

	} else {
		response.Error("非法的请求", "/role", c)
	}

}

//执行授权
func AdminDoRoleAuth(c *gin.Context) {
	roleId := c.PostForm("role_id")
	accessNode := c.PostFormArray("access_node")
	if roleId != "" {
		if err := admin.DoRoleAuth(roleId, accessNode); err == nil {
			response.Success("授权成功", "/role", c)
		} else {
			response.Error(err.Error(), "/role", c)
		}
	} else {
		response.Error("非法的请求", "/role", c)
	}

}
