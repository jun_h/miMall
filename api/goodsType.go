package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//部门类型
func AdminGoodsType(c *gin.Context) {
	if goodsTypeList, err := admin.GoodsType(); err == nil {
		c.HTML(http.StatusOK, "admin_goodsType_index.html", goodsTypeList)
	} else {
		response.Error("加载页面失败"+err.Error(), "", c)
	}

}

//增加类型
func AdminGoodsTypeAdd(c *gin.Context) {
	c.HTML(http.StatusOK, "admin_goodsType_add.html", gin.H{})
}

//增加类型操作
func AdminDoGoodsTypeAdd(c *gin.Context) {
	var rGoodsType request.AdminGoodsTypeRequest
	if err := c.ShouldBind(&rGoodsType); err == nil {
		if err := admin.DoGoodsTypeAdd(rGoodsType); err == nil {
			response.Success("添加类型成功", "/goodsType", c)
		} else {
			response.Error("添加类型失败"+err.Error(), "/goodsType/add", c)
		}
	} else {
		response.Error("非法的请求", "/goodsType/add", c)
	}

}

//编辑类型
func AdminGoodsTypeEdit(c *gin.Context) {

	if id := c.Query("id"); id != "" {
		if goodsType, err := admin.GoodsTypeEdie(id); err == nil {
			c.HTML(http.StatusOK, "admin_goodsType_edit.html", goodsType)
		} else {
			response.Error("加载页面失败"+err.Error(), "/goodsType", c)
		}
	} else {
		response.Error("非法的请求", "/goodsType", c)
	}
}

//编辑类型操作
func AdminDoGoodsTypeEdit(c *gin.Context) {
	var rGoodsType request.AdminGoodsTypeRequest
	if err := c.ShouldBind(&rGoodsType); err == nil {
		if err := admin.DoGoodsTypeEdie(rGoodsType); err == nil {
			response.Success("修改信息成功", "/goodsType", c)
		} else {
			response.Error("修改信息失败"+err.Error(), "/goodsType/add", c)
		}
	} else {
		response.Error("非法的请求", "/goodsType/add", c)
	}

}

//删除类型操作
func AdminGoodsTypeDelete(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if err := admin.GoodsTypeDelete(id); err == nil {
			response.Success("删除成功", "/goodsType", c)
		} else {
			response.Error("删除失败"+err.Error(), "/goodsType", c)
		}
	} else {
		response.Error("非法的请求", "/goodsType", c)
	}
}
