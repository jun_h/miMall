package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//轮播图列表
func AdminFocus(c *gin.Context) {
	if focusList, err := admin.Focus(); err == nil {
		c.HTML(http.StatusOK, "admin_focus_index.html", focusList)
	} else {
		response.Error(err.Error(), "", c)
	}

}

//增加轮播图
func AdminFocusAdd(c *gin.Context) {
	c.HTML(http.StatusOK, "admin_focus_add.html", gin.H{})
}

//增加轮播图操作
func AdminDoFocusAdd(c *gin.Context) {
	var rFocus request.AdminFocusRequest
	if err := c.ShouldBind(&rFocus); err == nil {
		if err := admin.DoFocusAdd(rFocus,c); err == nil {
			response.Success("添加轮播图成功", "/focus", c)
			return
		} else {
			response.Error(err.Error(), "/focus/add", c)
		}
	} else {
		response.Error("非法的请求", "/focus/add", c)
	}

}

//编辑轮播图
func AdminFocusEdit(c *gin.Context) {

	if id := c.Query("id"); id != "" {
		if focus, err := admin.FocusEdie(id); err == nil {
			c.HTML(http.StatusOK, "admin_focus_edit.html", focus)
		} else {
			response.Error(err.Error(), "/focus", c)
		}
	} else {
		response.Error("非法的请求", "/focus", c)
	}
}

//编辑轮播图操作
func AdminDoFocusEdit(c *gin.Context) {
	var rFocus request.AdminFocusRequest
	if err := c.ShouldBind(&rFocus); err == nil {
		if err := admin.DoFocusEdie(rFocus,c); err == nil {
			response.Success("修改轮播图信息成功", "/focus", c)
		} else {
			response.Error(err.Error(), "/focus", c)
		}
	} else {
		response.Error("非法的请求", "/focus", c)
	}

}

//删除轮播图操作
func AdminFocusDelete(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if err := admin.FocusDelete(id); err == nil {
			response.Success("删除轮播图成功", "/focus", c)
		} else {
			response.Error(err.Error(), "/focus", c)
		}
	} else {
		response.Error("非法的请求", "/focus", c)
	}
}