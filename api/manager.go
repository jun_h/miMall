package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//管理员列表
func AdminManager(c *gin.Context) {
	if managerList, err := admin.Manager(); err == nil {
		c.HTML(http.StatusOK, "admin_manager_index.html", managerList)
	} else {
		response.Error(err.Error(), "", c)
	}

}

//增加管理员
func AdminManagerAdd(c *gin.Context) {

	if roleList, err := admin.ManagerAdd(); err == nil {
		c.HTML(http.StatusOK, "admin_manager_add.html", roleList)
	} else {
		response.Error(err.Error(), "", c)
	}

}

//增加管理员操作
func AdminDoManagerAdd(c *gin.Context) {
	var rManager request.AdminManagerRequest
	if err := c.ShouldBind(&rManager); err == nil {
		if err := admin.DoManagerAdd(rManager); err == nil {
			response.Success("添加管理员成功", "/manager", c)
			return
		} else {
			response.Error(err.Error(), "/manager/add", c)
		}
	} else {
		response.Error("非法的请求", "/manager/add", c)
	}

}

//编辑管理员
func AdminManagerEdit(c *gin.Context) {

	if id := c.Query("id"); id != "" {
		if manager,roleList,err := admin.ManagerEdie(id);err==nil{
			c.HTML(http.StatusOK, "admin_manager_edit.html",gin.H{
				"manager":manager,
				"roleList":roleList,
			} )
		}else {
			response.Error(err.Error(), "/manager", c)
		}
	}else {
		response.Error("非法的请求", "/manager", c)
	}
}

//编辑管理员操作
func AdminDoManagerEdit(c *gin.Context) {
	var rManager request.AdminManagerRequest
	if err := c.ShouldBind(&rManager); err == nil {
		if err := admin.DoManagerEdie(rManager); err == nil {
			response.Success("修改管理员信息成功", "/manager", c)
		} else {
			response.Error(err.Error(), "/manager/add", c)
		}
	} else {
		response.Error("非法的请求", "/manager/add", c)
	}

}

//删除管理员操作
func AdminManagerDelete(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if err := admin.ManagerDelete(id);err==nil{
			response.Success("删除管理员成功", "/manager", c)
		}else {
			response.Error(err.Error(), "/manager", c)
		}
	}else {
		response.Error("非法的请求", "/manager", c)
	}
}
