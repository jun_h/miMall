package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//权限列表
func AdminAccess(c *gin.Context) {
	if accessList, err := admin.Access(); err == nil {
		c.HTML(http.StatusOK, "admin_access_index.html", accessList)
	} else {
		response.Error(err.Error(), "", c)
	}

}

//增加权限
func AdminAccessAdd(c *gin.Context) {

	if accessList, err := admin.AccessAdd(); err == nil {
		c.HTML(http.StatusOK, "admin_access_add.html", accessList)
	} else {
		response.Error(err.Error(), "", c)
	}

}

//增加权限操作
func AdminDoAccessAdd(c *gin.Context) {
	var rAccess request.AdminAccessRequest
	if err := c.ShouldBind(&rAccess); err == nil {
		if err := admin.DoAccessAdd(rAccess); err == nil {
			response.Success("添加模块成功", "/access", c)
			return
		} else {
			response.Error(err.Error(), "/access/add", c)
		}
	} else {
		response.Error("非法的请求"+err.Error(), "/access/add", c)
	}

}

//编辑管理员
func AdminAccessEdit(c *gin.Context) {

	if id := c.Query("id"); id != "" {
		if access, accessList, err := admin.AccessEdie(id); err == nil {
			c.HTML(http.StatusOK, "admin_access_edit.html", gin.H{
				"access":     access,
				"accessList": accessList,
			})
		} else {
			response.Error(err.Error(), "/access", c)
		}
	} else {
		response.Error("非法的请求", "/access", c)
	}
}

//编辑管理员操作
func AdminDoAccessEdit(c *gin.Context) {
	var rAccess request.AdminAccessRequest
	if err := c.ShouldBind(&rAccess); err == nil {
		if err := admin.DoAccessEdie(rAccess); err == nil {
			response.Success("修改模块信息成功", "/access", c)
		} else {
			response.Error(err.Error(), "/access", c)
		}
	} else {
		response.Error("非法的请求", "/access", c)
	}

}

//删除管理员操作
func AdminAccessDelete(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if err := admin.AccessDelete(id); err == nil {
			response.Success("删除模块成功", "/access", c)
		} else {
			response.Error(err.Error(), "/access", c)
		}
	} else {
		response.Error("非法的请求", "/access", c)
	}
}
