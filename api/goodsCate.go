package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xiaomi/global/response"
	"xiaomi/models/request"
	"xiaomi/service/admin"
)

//商品分类列表
func AdminGoodsCate(c *gin.Context) {
	if goodsCateList, err := admin.GoodsCate(); err == nil {
		c.HTML(http.StatusOK, "admin_goodsCate_index.html", goodsCateList)
	} else {
		response.Error("加载列表失败："+err.Error(), "", c)
	}

}

//增加商品分类
func AdminGoodsCateAdd(c *gin.Context) {

	if goodsCateList, err := admin.GoodsCateAdd(); err == nil {
		c.HTML(http.StatusOK, "admin_goodsCate_add.html", goodsCateList)
	} else {
		response.Error("加载页面失败："+err.Error(), "", c)
	}

}

//增加商品分类操作
func AdminDoGoodsCateAdd(c *gin.Context) {
	var rGoodsCate request.AdminGoodsCateRequest
	if err := c.ShouldBind(&rGoodsCate); err == nil {
		if err := admin.DoGoodsCateAdd(rGoodsCate,c); err == nil {
			response.Success("添加商品分类成功", "/goodsCate", c)
			return
		} else {
			response.Error("增加分类失败："+err.Error(), "/goodsCate/add", c)
		}
	} else {
		response.Error("非法的请求："+err.Error(), "/goodsCate/add", c)
	}

}

//编辑商品分类
func AdminGoodsCateEdit(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if goodsCate, goodsCateList, err := admin.GoodsCateEdie(id); err == nil {
			c.HTML(http.StatusOK, "admin_goodsCate_edit.html", gin.H{
				"goodsCate":     goodsCate,
				"goodsCateList": goodsCateList,
			})
		} else {
			response.Error("加载页面失败："+err.Error(), "/goodsCate", c)
		}
	} else {
		response.Error("非法的请求", "/goodsCate", c)
	}
}

//编辑商品分类操作
func AdminDoGoodsCateEdit(c *gin.Context) {
	var rGoodsCate request.AdminGoodsCateRequest
	if err := c.ShouldBind(&rGoodsCate); err == nil {
		if err := admin.DoGoodsCateEdie(rGoodsCate,c); err == nil {
			response.Success("修改商品分类信息成功", "/goodsCate", c)
		} else {
			response.Error("编辑分类失败："+err.Error(), "/goodsCate", c)
		}
	} else {
		response.Error("非法的请求", "/goodsCate", c)
	}

}

//删除商品分类操作
func AdminGoodsCateDelete(c *gin.Context) {
	if id := c.Query("id"); id != "" {
		if err := admin.GoodsCateDelete(id); err == nil {
			response.Success("删除成功", "/goodsCate", c)
		} else {
			response.Error("删除失败："+err.Error(), "/goodsCate", c)
		}
	} else {
		response.Error("非法的请求", "/goodsCate", c)
	}
}
