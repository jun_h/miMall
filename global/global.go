package global

import (

	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"xiaomi/config"
)

var (
	MysqlDb    *gorm.DB
	InitConfig config.Server
	RedisDb    *redis.Client
)
