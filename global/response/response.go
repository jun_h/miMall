package response

import (
	"github.com/gin-gonic/gin"
	"strings"
)

// 三位数错误编码为复用http原本含义
// 五位数错误编码为应用自定义错误
// 五开头的五位数错误编码为服务器端错误，比如数据库操作失败
// 四开头的五位数错误编码为客户端错误，有时候是客户端代码写错了，有时候是用户操作错误
const (
	//SuccessCode 操作成功
	SuccessCode = 200
	// CodeCheckLogin 未登录
	CodeCheckLogin = 401
	// CodeNoRightErr 未授权访问
	CodeNoRightErr = 403
	// CodeDBError 数据库操作失败
	CodeDBError = 50001
	// CodeEncryptError 加密失败
	CodeEncryptError = 50002
	//CodeParamErr 各种奇奇怪怪的参数错误
	CodeParamErr = 40001
)

// response 基础序列化器
type Response struct {
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Data  interface{} `json:"data,omitempty"`
	Error string      `json:"err,omitempty"`
}

func Success(message, redirect string, c *gin.Context) {
	if strings.Contains(redirect, "http:") {
		c.HTML(302, "admin_success.html", gin.H{
			"message":  message,
			"redirect": redirect,
		})
	} else {
		c.HTML(302, "admin_success.html", gin.H{
			"message":  message,
			"redirect": "/admin" + redirect,
		})
	}
}

func Error(message, redirect string, c *gin.Context) {
	if strings.Contains(redirect, "http:") {
		c.HTML(302, "admin_err.html", gin.H{
			"message":  message,
			"redirect": redirect,
		})
	} else {
		c.HTML(302, "admin_err.html", gin.H{
			"message":  message,
			"redirect": "/admin" + redirect,
		})
	}

}



/*
// TrackedErrorResponse 有追踪信息的错误响应
type TrackedErrorResponse struct {
	response
	TrackID string `json:"track_id"`
}
*/

/*
func result(code int, data interface{}, msg, err string, c *gin.Context) {
	response := &Response{
		Code:  code,
		Data:  data,
		Msg:   msg,
		Error: err,
	}
	c.JSON(SuccessCode, response)
}
//OK 操作成功
func OK(c *gin.Context) {
	result(SuccessCode, "", "操作成功", "", c)
}

//OK 写消息
func OkWithMessage(message string, c *gin.Context) {
	result(SuccessCode, "", message, "", c)
}

//OK 写数据
func OkWithData(data interface{}, c *gin.Context) {
	result(SuccessCode, data, "操作成功", "", c)
}

//OK 写消息和数据
func OkDetailed(data interface{}, message string, c *gin.Context) {
	result(SuccessCode, data, message, "", c)
}

// CheckLogin 检查登录
func CheckLogin(c *gin.Context) {
	result(CodeCheckLogin, "", "未登录", "", c)
}

//权限不足
func NoRightErr(err string, c *gin.Context) {
	c.HTML(CodeNoRightErr, "error.html", gin.H{"Msg": "操作失败", "Error": err})
}

//fail 返回错误
func Fail(c *gin.Context) {
	result(CodeParamErr, "", "操作失败", "", c)
}

//fail 返回提示
func FailWithMessage(message string, c *gin.Context) {
	result(CodeParamErr, "", message, "", c)
}

//fail 返回错误
func FailWithError(err string, c *gin.Context) {
	result(CodeParamErr, "", "操作失败", err, c)
}

//fail 返回数据
func FailWithData(data interface{}, c *gin.Context) {
	result(CodeParamErr, data, "操作失败", "", c)
}

//fail 返回数据 提示 错误
func FailWithDetailed(code int, data interface{}, message, err string, c *gin.Context) {
	result(CodeParamErr, data, message, err, c)
}*/

/*// Err 通用错误处理
func Err(errCode int, msg string, err error) *response {
	res := &response{
		Code:  errCode,
		Msg:   msg,
		Error: err.Error(),
	}
	// 生产环境隐藏底层报错
	if err != nil && gin.Mode() != gin.ReleaseMode {
		res.Error = err.Error()
	}
	return res
}

// DBErr 数据库操作失败
func DBErr(msg string, err error) *response {
	if msg == "" {
		msg = "数据库操作失败"
	}
	return Err(CodeDBError, msg, err)
}

// ParamErr 各种参数错误
func ParamErr(msg string, err error) *response {
	if msg == "" {
		msg = "参数错误"
	}
	return Err(CodeParamErr, msg, err)
}*/
