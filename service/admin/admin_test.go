package admin

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"testing"
	"xiaomi/global"
)

func init() {
	db, err := gorm.Open("mysql", "root:123456@/mi_mall?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("数据库连接失败")
	}
	global.MysqlDb = db
}

func TestManagerEdie(t *testing.T) {
	if manager, role,err := ManagerEdie("1");err!=nil{
		t.Log(err)
	}else {
		t.Log(manager)
		t.Log(role)
	}
	global.MysqlDb.Close()
}

func TestRoleAuth(t *testing.T) {
	if manager, err := RoleAuth("1");err!=nil{
		t.Log(err)
	}else {
		t.Log(manager)
	}
	global.MysqlDb.Close()
}