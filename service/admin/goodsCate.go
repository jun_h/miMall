package admin

import (
	"errors"
	"github.com/gin-gonic/gin"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/utils"
)

func GoodsCate() ([]models.GoodsCate, error) {
	var goodsCateList []models.GoodsCate

	if err := global.MysqlDb.Preload("GoodsCateItem").Where("pid=0").Find(&goodsCateList).
		Error; err != nil {
		return nil, err
	}
	return goodsCateList, nil
}

func GoodsCateAdd() ([]models.GoodsCate, error) {
	var goodsCateList []models.GoodsCate
	if err := global.MysqlDb.Where("pid=0").Find(&goodsCateList).Error; err != nil {
		return nil, err
	}
	return goodsCateList, nil
}

func DoGoodsCateAdd(rGoodsCate request.AdminGoodsCateRequest, c *gin.Context) error {
	//接收上传图片
	uploadDir, _ := UploadImg("cate_img", c)
	//创建管理员模型
	goodsCate := models.GoodsCate{
		Title:       rGoodsCate.Title,
		Pid:         rGoodsCate.Pid,
		SubTitle:    rGoodsCate.SubTitle,
		Link:        rGoodsCate.Link,
		Template:    rGoodsCate.Template,
		Keywords:    rGoodsCate.Keywords,
		CateImg:     uploadDir,
		Description: rGoodsCate.Description,
		Sort:        rGoodsCate.Sort,
		Status:      rGoodsCate.Status,
		AddTime:     utils.GetUnix(),
	}

	return global.MysqlDb.Create(&goodsCate).Error
}

func GoodsCateEdie(id string) (*models.GoodsCate, []models.GoodsCate, error) {
	//查出需要修改的模块
	var goodsCate models.GoodsCate
	if err := global.MysqlDb.First(&goodsCate, id).Error; err != nil {
		return nil, nil, err
	}
	//查出顶级模块
	var goodsCateList []models.GoodsCate
	if err := global.MysqlDb.Where("pid=0").Find(&goodsCateList).Error; err != nil {
		return nil, nil, err
	}
	return &goodsCate, goodsCateList, nil
}

func DoGoodsCateEdie(rGoodsCate request.AdminGoodsCateRequest,c *gin.Context) error {
	//判断传入参数是否合法
	if rGoodsCate.Id == 0 {
		return errors.New("非法的请求")
	}
	//查出需要修改的用户
	var goodsCate models.GoodsCate
	if err := global.MysqlDb.First(&goodsCate, rGoodsCate.Id).Error; err != nil {
		return err
	}
	if uploadDir, _ := UploadImg("cate_img",c);uploadDir!=""{
		goodsCate.CateImg = uploadDir
	}

	goodsCate.Title = rGoodsCate.Title
	goodsCate.Pid = rGoodsCate.Pid
	goodsCate.Link = rGoodsCate.Link
	goodsCate.Template = rGoodsCate.Template
	goodsCate.SubTitle = rGoodsCate.SubTitle
	goodsCate.Keywords = rGoodsCate.Keywords
	goodsCate.Description = rGoodsCate.Description
	goodsCate.Sort = rGoodsCate.Sort
	goodsCate.Status = rGoodsCate.Status

	return global.MysqlDb.Save(&goodsCate).Error
}

func GoodsCateDelete(id string) error {
	//获取当前数据
	goodsCate := models.GoodsCate{}
	global.MysqlDb.Find(&goodsCate, id)
	if goodsCate.Pid == 0 { //顶级模块
		count := 0
		if err := global.MysqlDb.Model(&models.GoodsCate{}).Where("module_id=?", goodsCate.Id).
			Count(&count).Error; err != nil || count > 0 {
			return errors.New("当前分类下面还有菜单或者操作，无法删除")
		}
	}

	return global.MysqlDb.Delete(&goodsCate).Error
}
