package admin

import (
	"errors"
	"github.com/gin-gonic/gin"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/utils"
)

func Focus() ([]models.Focus, error) {
	var focusList []models.Focus
	if err := global.MysqlDb.Find(&focusList).
		Error; err != nil {
		return nil, err
	}
	return focusList, nil
}

//func FocusAdd() ([]models.Focus, error) {
//
//	return focusList, nil
//}

func DoFocusAdd(rFocus request.AdminFocusRequest, c *gin.Context) error {

	//执行图片上传
	focusImgSrc, _ := UploadImg("focus_img", c)

	//创建管理员模型
	focus := models.Focus{
		Title:     rFocus.Title,
		FocusType: rFocus.FocusType,
		FocusImg:  focusImgSrc,
		Link:      rFocus.Link,
		Sort:      rFocus.Sort,
		Status:    models.Active,
		AddTime:   utils.GetUnix(),
	}

	if err := global.MysqlDb.Create(&focus).Error; err != nil {
		return errors.New("增加轮播图失败：" + err.Error())
	}

	return nil
}

func FocusEdie(id string) (*models.Focus, error) {
	//查出需要修改的模块
	var focus models.Focus
	if err := global.MysqlDb.First(&focus, id).Error; err != nil {
		return nil, errors.New("轮播图不存在" + err.Error())
	}

	return &focus, nil
}

func DoFocusEdie(rFocus request.AdminFocusRequest, c *gin.Context) error {
	//判断传入参数是否合法
	if rFocus.Id == 0 {
		return errors.New("非法的请求")
	}
	//查出需要修改的用户
	var focus models.Focus
	if err := global.MysqlDb.First(&focus, rFocus.Id).Error; err != nil {
		return errors.New("用户不存在")
	}
	//执行图片上传
	if focusImgSrc, _ := UploadImg("focus_img", c); focusImgSrc != "" {
		focus.FocusImg = focusImgSrc
	}

	focus.Title = rFocus.Title
	focus.FocusType = rFocus.FocusType
	focus.Link = rFocus.Link
	focus.Sort = rFocus.Sort
	focus.Status = rFocus.Status

	if err := global.MysqlDb.Save(&focus).Error; err != nil {
		return errors.New("修改数据失败：" + err.Error())
	}
	return nil
}

func FocusDelete(id string) error {
	//获取当前数据
	return global.MysqlDb.Where("id=?", id).Delete(&models.Focus{}).Error
}
