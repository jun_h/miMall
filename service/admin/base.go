package admin

import (
	"errors"
	"github.com/gin-gonic/gin"
	"os"
	"path"
	"strconv"
	"xiaomi/utils"
)

func UploadImg(picName string, c *gin.Context) (string, error) {
	//1、获取上传的文件
	h, err := c.FormFile(picName)

	if err != nil {
		return "", err
	}
	//2、获取后缀名 判断类型是否正确  .jpg .png .gif .jpeg
	extName := path.Ext(h.Filename)

	allowExtMap := map[string]bool{
		".jpg":  true,
		".png":  true,
		".gif":  true,
		".jpeg": true,
	}

	if _, ok := allowExtMap[extName]; !ok {
		return "", errors.New("图片后缀名不合法")
	}
	//4、创建图片保存目录  static/upload/20200623
	day := utils.GetDay()
	dir := "static/upload/" + day

	if err := os.MkdirAll(dir, 0666); err != nil {
		return "", errors.New("系统错误："+err.Error())
	}
	//5、生成文件名称   144325235235.png
	fileUnixName := strconv.FormatInt(utils.GetUnixNano(), 10)
	//static/upload/20200623/144325235235.png
	saveDir := path.Join(dir, fileUnixName+extName)
	//6、保存图片
	err = c.SaveUploadedFile(h, saveDir)

	return saveDir, nil
}
