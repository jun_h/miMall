package admin

import (
	"errors"
	"github.com/jinzhu/gorm"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
)

func Index(manager *models.Manager) (map[string]interface{}, error) {
	//1、获取全部的权限 (排序)，忘记的话参考：https://gorm.io/zh_CN/docs/preload.html
	var access []models.Access
	if err := global.MysqlDb.Preload("AccessItem", func(db *gorm.DB) *gorm.DB {
		return db.Order("access.sort DESC")
	}).Order("sort desc").Where("module_id=?", 0).Find(&access).Error; err != nil {
		return nil, errors.New("数据库操作失败")
	}

	//2、获取当前角色拥有的权限 ，并把权限id放在一个map对象里面
	var roleAccess []models.RoleAccess
	if err := global.MysqlDb.Where("role_id=?", manager.RoleId).Find(&roleAccess).Error; err != nil {
		return nil, errors.New("数据库操作失败")
	}
	roleAccessMap := make(map[int]int)
	for _, v := range roleAccess {
		roleAccessMap[v.AccessId] = v.AccessId
	}
	//3、循环遍历所有的权限数据，判断当前权限的id是否在角色权限的Map对象中,如果是的话给当前数据加入checked属性
	for i := 0; i < len(access); i++ {
		if _, ok := roleAccessMap[access[i].Id]; ok {
			access[i].Checked = true
		}
		for j := 0; j < len(access[i].AccessItem); j++ {
			if _, ok := roleAccessMap[access[i].AccessItem[j].Id]; ok {
				access[i].AccessItem[j].Checked = true
			}
		}
	}
	m := make(map[string]interface{})
	m["username"] = manager.Username
	m["accessList"] = access
	m["isSuper"] = manager.IsSuper

	return m, nil
}

func ChangeStatus(index request.AdminIndexRequest) error {
	if index.Field != "status" {
		return errors.New("非法的操作")
	}
	return global.MysqlDb.Exec("update "+index.Table+" set "+index.Field+"=ABS("+index.Field+"-1) where id=?", index.Id).Error
}

func EditNum(index request.AdminIndexRequest) error {
	if index.Field != "sort"{
		return errors.New("非法的操作")
	}

	return global.MysqlDb.Exec("update "+index.Table+" set "+index.Field+"=? where id=?", index.Num, index.Id).Error
}
