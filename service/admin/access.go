package admin

import (
	"errors"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/utils"
)

func Access() ([]models.Access, error) {
	var accessList []models.Access

	if err := global.MysqlDb.Preload("AccessItem").Where("module_id=0").Find(&accessList).
		Error; err != nil {
		return nil, err
	}
	return accessList, nil
}

func AccessAdd() ([]models.Access, error) {
	var accessList []models.Access
	if err := global.MysqlDb.Where("module_id=0").Find(&accessList).Error; err != nil {
		return nil, err
	}
	return accessList, nil
}

func DoAccessAdd(rAccess request.AdminAccessRequest) error {
	//判断数据库里面有没有当前用户

	//创建管理员模型
	access := models.Access{
		ModuleName:  rAccess.ModuleName,
		ActionName:  rAccess.ActionName,
		Type:        rAccess.Type,
		Url:         rAccess.Url,
		ModuleId:    rAccess.ModuleId,
		Sort:        rAccess.Sort,
		Description: rAccess.Description,
		Status:      models.Active,
		AddTime:     utils.GetUnix(),
	}

	if err := global.MysqlDb.Create(&access).Error; err != nil {
		return errors.New("增加模块失败")
	}

	return nil
}

func AccessEdie(id string) (*models.Access, []models.Access, error) {
	//查出需要修改的模块
	var access models.Access
	if err := global.MysqlDb.First(&access, id).Error; err != nil {
		return nil, nil, errors.New("模块不存在" + err.Error())
	}
	//查出顶级模块
	var accessList []models.Access
	if err := global.MysqlDb.Where("module_id=0").Find(&accessList).Error; err != nil {
		return nil, nil, errors.New("数据操作失败" + err.Error())
	}
	return &access, accessList, nil
}

func DoAccessEdie(rAccess request.AdminAccessRequest) error {
	//判断传入参数是否合法
	if rAccess.Id == 0 {
		return errors.New("非法的请求")
	}
	//查出需要修改的用户
	var access models.Access
	if err := global.MysqlDb.First(&access, rAccess.Id).Error; err != nil {
		return errors.New("用户不存在")
	}

	access.ModuleName = rAccess.ModuleName
	access.Type = rAccess.Type
	access.ActionName = rAccess.ActionName
	access.Url = rAccess.Url
	access.ModuleId = rAccess.ModuleId
	access.Sort = rAccess.Sort
	access.Description = rAccess.Description
	access.Status = rAccess.Status

	if err := global.MysqlDb.Save(&access).Error; err != nil {
		return errors.New("修改数据失败：" + err.Error())
	}

	return nil
}

func AccessDelete(id string) error {
	//获取当前数据
	access := models.Access{}
	global.MysqlDb.Find(&access, id)
	if access.ModuleId == 0 { //顶级模块
		count := 0
		if err := global.MysqlDb.Model(&models.Access{}).Where("module_id=?", access.Id).
			Count(&count).Error; err != nil || count > 0 {
			return errors.New("当前模块下面还有菜单或者操作，无法删除")
		}
	}



	return global.MysqlDb.Delete(&access).Error
}
