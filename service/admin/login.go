package admin

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/dchest/captcha"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
)


//产生验证码
func Captcha(c *gin.Context) {
	l := 4          //验证码长度
	w, h := 120, 40 //图片大小
	captchaId := captcha.NewLen(l)
	session := sessions.Default(c)
	fmt.Println("+++++++++++++++++")
	fmt.Println(captchaId)
	session.Set("captcha", captchaId)
	_ = session.Save()
	_ = Serve(c.Writer, c.Request, captchaId, ".png", "zh", false, w, h)
}
//保存验证码
func Serve(w http.ResponseWriter, r *http.Request, id, ext, lang string, download bool, width, height int) error {
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("Pragma", "no-cache")
	w.Header().Set("Expires", "0")

	var content bytes.Buffer
	switch ext {
	case ".png":
		w.Header().Set("Content-Type", "image/png")
		_ = captcha.WriteImage(&content, id, width, height)
	case ".wav":
		w.Header().Set("Content-Type", "audio/x-wav")
		_ = captcha.WriteAudio(&content, id, lang)
	default:
		return captcha.ErrNotFound
	}

	if download {
		w.Header().Set("Content-Type", "application/octet-stream")
	}
	http.ServeContent(w, r, id+ext, time.Time{}, bytes.NewReader(content.Bytes()))
	return nil
}
//验证验证码
func CaptchaVerify(c *gin.Context, code string) bool {
	session := sessions.Default(c)
	if captchaId := session.Get("captcha"); captchaId != nil {
		session.Delete("captcha")
		_ = session.Save()
		if captcha.VerifyString(captchaId.(string), code) {
			return true
		} else {
			return false
		}
	} else {
		return false
	}
}

// setSession 设置session
func setSession(c *gin.Context, user models.Manager) error {
	s := sessions.Default(c)
	s.Clear()
	s.Set("user_id", user.Id)
	return s.Save()
}
//登录
func DoLogin(c *gin.Context, login request.AdminLoginRequest) error {
	if !CaptchaVerify(c, login.Captcha) {
		return errors.New("验证码错误")
	}
	//根据用户名查出用户
	var manager models.Manager
	if err := global.MysqlDb.Where("username=? ", login.Username).
		First(&manager).Error; err != nil {
		return errors.New("用户不存在")
	}
	//校验密码
	if err := manager.CheckPassword(login.Password); err != nil {
		return errors.New("账号或密码错误")
	}
	//设置session
	_ = setSession(c, manager)

	return nil

}
//退出登录
func LoginOut(c *gin.Context) error {
	//清除sessions
	s := sessions.Default(c)
	s.Clear()
	return s.Save()
}
