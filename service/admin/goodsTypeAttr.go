package admin

import (
	"errors"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/utils"
)

func GoodsTypeAttr(cateId string) (*models.GoodsType, []models.GoodsTypeAttribute, error) {
	//获取类别信息
	var goodsType models.GoodsType
	if err := global.MysqlDb.First(&goodsType, cateId).Error; err != nil {
		return nil, nil, err
	}

	//查询当前类型下面的商品类型属性
	var goodsTypeAttr []models.GoodsTypeAttribute
	if err := global.MysqlDb.Where("cate_id=?", cateId).Find(&goodsTypeAttr).Error; err != nil {
		return nil, nil, err
	}
	return &goodsType, goodsTypeAttr, nil
}

func GoodsTypeAttrAdd(cateId int) ([]models.GoodsType, error) {
	//获取全部的类别信息
	var goodsTypeList []models.GoodsType
	if err := global.MysqlDb.Find(&goodsTypeList).Error; err != nil {
		return nil, err
	}

	return goodsTypeList, nil
}

func DoGoodsTypeAttrAdd(rGoodsTypeAttr request.AdminGoodsTypeAttrRequest) error {
	//创建部门模型
	goodsTypeAttr := models.GoodsTypeAttribute{
		Title:     rGoodsTypeAttr.Title,
		CateId:    rGoodsTypeAttr.CateId,
		AttrType:  rGoodsTypeAttr.AttrType,
		AttrValue: rGoodsTypeAttr.AttrValue,
		Status:    models.Active,
		Sort:      rGoodsTypeAttr.CateId,
		AddTime:   utils.GetUnix(),
	}

	return global.MysqlDb.Create(&goodsTypeAttr).Error
}

func GoodsTypeAttrEdie(id string) ([]models.GoodsType, *models.GoodsTypeAttribute, error) {
	//查出所有类型
	var GoodsType []models.GoodsType
	if err := global.MysqlDb.Find(&GoodsType).Error; err != nil {
		return nil,nil, err
	}
	//获取类型对应属性
	var goodsTypeAttr models.GoodsTypeAttribute
	if err := global.MysqlDb.First(&goodsTypeAttr, id).Error; err != nil {
		return nil, nil,err
	}

	return GoodsType, &goodsTypeAttr, nil
}

func DoGoodsTypeAttrEdie(rGoodsTypeAttr request.AdminGoodsTypeAttrRequest) error {
	//查出需要修改的用户
	if rGoodsTypeAttr.Id == 0 {
		return errors.New("非法的请求")
	}
	var goodsTypeAttr models.GoodsTypeAttribute
	if err := global.MysqlDb.First(&goodsTypeAttr, rGoodsTypeAttr.Id).Error; err != nil {
		return err
	}
	goodsTypeAttr.Title = rGoodsTypeAttr.Title
	goodsTypeAttr.CateId = rGoodsTypeAttr.CateId
	goodsTypeAttr.AttrType = rGoodsTypeAttr.AttrType
	goodsTypeAttr.AttrValue = rGoodsTypeAttr.AttrValue
	goodsTypeAttr.Sort = rGoodsTypeAttr.Sort

	return global.MysqlDb.Save(&goodsTypeAttr).Error
}

func GoodsTypeAttrDelete(id string) error {
	return global.MysqlDb.Where("id = ?", id).Delete(&models.GoodsTypeAttribute{}).Error
}
