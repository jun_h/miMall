package admin

import (
	"errors"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/utils"
)

func Manager() ([]models.Manager, error) {
	var managerList []models.Manager
	if err := global.MysqlDb.Preload("Role").Find(&managerList).Error; err != nil {
		return nil, err
	}
	return managerList, nil
}

func ManagerAdd() ([]models.Role, error) {

	var role []models.Role
	if err := global.MysqlDb.Find(&role).Error; err != nil {
		return nil, err
	}
	return role, nil
}

func DoManagerAdd(rManager request.AdminManagerRequest) error {
	//判断传入参数是否合法
	if err := utils.RegexpAll(rManager.Username, rManager.Email, rManager.Mobile, rManager.Password); err != nil {
		return err
	}
	//判断数据库里面有没有当前用户
	count := 0
	if err := global.MysqlDb.Model(&models.Manager{}).Where("username=?", rManager.Username).
		Count(&count).Error; err != nil || count > 0 {
		return errors.New("用户名已经存在")
	}

	//创建管理员模型
	manager := models.Manager{
		Username: rManager.Username,
		Mobile:   rManager.Mobile,
		Email:    rManager.Email,
		Status:   models.Active,
		AddTime:  utils.GetUnix(),
		RoleId:   rManager.RoleId,
	}
	//加密密码
	if err := manager.SetPassword(rManager.Password); err != nil {
		return errors.New("系统故障")
	}

	if err := global.MysqlDb.Create(&manager).Error; err != nil {
		return errors.New("增加管理员失败")
	}

	return nil
}

func ManagerEdie(id string) (*models.Manager,[]models.Role, error) {
	var manager models.Manager
	if err := global.MysqlDb.First(&manager, id).Error; err != nil {
		return nil,nil, errors.New("用户不存在"+err.Error())
	}
	var role []models.Role
	if err := global.MysqlDb.Find(&role).Error; err != nil {
		return nil,nil, errors.New("数据操作失败"+err.Error())
	}
	return &manager,role, nil
}

func DoManagerEdie(rManager request.AdminManagerRequest) error {
	//判断传入参数是否合法
	if err := utils.RegexpPhone(rManager.Mobile); err != nil {
		return err
	} else if err := utils.RegexpEmail(rManager.Email); err != nil {
		return err
	} else if rManager.Password != "" {
		if err := utils.RegexpPassword(rManager.Password); err != nil {
			return err
		}
	}

	//查出需要修改的用户
	var manager models.Manager
	if err := global.MysqlDb.First(&manager, rManager.Id).Error; err != nil {
		return errors.New("用户不存在")
	}
	//加密密码
	if err := manager.SetPassword(rManager.Password); err != nil {
		return errors.New("系统故障")
	}

	manager.RoleId = rManager.RoleId
	manager.Mobile = rManager.Mobile
	manager.Email = rManager.Email

	if err := global.MysqlDb.Save(&manager).Error; err != nil {
		return errors.New("修改数据失败：" + err.Error())
	}

	return nil
}

func ManagerDelete(id string) error {

	if err := global.MysqlDb.Where("id = ?", id).Delete(&models.Manager{}).Error;err!=nil{
		return errors.New("删除管理员失败："+err.Error())
	}

	return nil
}
