package admin

import (
	"errors"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/utils"
)

//部门
func GoodsType() ([]models.GoodsType, error) {
	var goodsTypeList []models.GoodsType
	if err := global.MysqlDb.Find(&goodsTypeList).Error; err != nil {
		return nil, err
	}
	return goodsTypeList, nil
}

func DoGoodsTypeAdd(rGoodsType request.AdminGoodsTypeRequest) error {
	//判断数据库里面有没有当前部门
	count := 0
	if err := global.MysqlDb.Model(&models.GoodsType{}).Where("title=?", rGoodsType.Title).
		Count(&count).Error; err != nil || count > 0 {
		return errors.New("类型已存在已经存在")
	}

	//创建类型模型
	goodsType := models.GoodsType{
		Title:       rGoodsType.Title,
		Description: rGoodsType.Description,
		Status:      rGoodsType.Status,
		AddTime:     utils.GetUnix(),
	}

	return global.MysqlDb.Create(&goodsType).Error
}

func GoodsTypeEdie(id string) (*models.GoodsType, error) {
	var goodsType models.GoodsType
	if err := global.MysqlDb.First(&goodsType, id).Error; err != nil {
		return nil, err
	}
	return &goodsType, nil
}

func DoGoodsTypeEdie(rGoodsType request.AdminGoodsTypeRequest) error {
	//查出需要修改的用户
	if rGoodsType.Id == 0 {
		return errors.New("非法的请求")
	}
	var goodsType models.GoodsType
	if err := global.MysqlDb.First(&goodsType, rGoodsType.Id).Error; err != nil {
		return err
	}
	goodsType.Title = rGoodsType.Title
	goodsType.Description = rGoodsType.Description
	goodsType.Status=rGoodsType.Status

	return global.MysqlDb.Save(&goodsType).Error
}

func GoodsTypeDelete(id string) error {
	return global.MysqlDb.Where("id = ?", id).Delete(&models.GoodsType{}).Error
}