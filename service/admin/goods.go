package admin

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
	"strings"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/utils"
)

//商品
func Goods(pageId, pageSize int, keyword string) ([]models.Goods, int, error) {
	//获取所有的商品
	var goodsList []models.Goods
	where := "1=1"
	if keyword != "" {
		where += " AND title like \"%" + keyword + "%\""
	}
	if err := global.MysqlDb.Where(where).Offset((pageId - 1) * pageSize).Limit(pageSize).Find(&goodsList).Error; err != nil {
		return nil, 0, err
	}
	//查询goods表里面的数量
	var count int
	global.MysqlDb.Where(where).Table("goods").Count(&count)

	return goodsList, count, nil
}

func GoodsAdd() ([]models.GoodsColor, []models.GoodsCate, []models.GoodsType, error) {
	//获取所有的颜色
	var goodsColorList []models.GoodsColor
	if err := global.MysqlDb.Find(&goodsColorList).Error; err != nil {
		return nil, nil, nil, err
	}
	//获取所有的分类
	var goodsCateList []models.GoodsCate
	if err := global.MysqlDb.Find(&goodsCateList).Error; err != nil {
		return nil, nil, nil, err
	}
	//获取所有的类型
	var goodsTypeList []models.GoodsType
	if err := global.MysqlDb.Find(&goodsTypeList).Error; err != nil {
		return nil, nil, nil, err
	}
	return goodsColorList, goodsCateList, goodsTypeList, nil
}

func DoGoodsAdd(rGoods request.AdminGoodsRequest, c *gin.Context) error {

	//获取颜色信息 把颜色转化成字符串
	goodsColorStr := strings.Join(rGoods.GoodsColor, ",")
	//上传图片   生成缩略图
	goodsImg, _ := UploadImg("goods_img", c)
	//增加商品数据
	goods := models.Goods{
		Title:         rGoods.Title,
		SubTitle:      rGoods.SubTitle,
		GoodsSn:       rGoods.GoodsSn,
		CateId:        rGoods.CateId,
		ClickCount:    100,
		GoodsNumber:   rGoods.GoodsNumber,
		MarketPrice:   rGoods.MarketPrice,
		Price:         rGoods.Price,
		RelationGoods: rGoods.RelationGoods,
		GoodsAttr:     rGoods.GoodsAttr,
		GoodsVersion:  rGoods.GoodsVersion,
		GoodsGift:     rGoods.GoodsGift,
		GoodsFitting:  rGoods.GoodsFitting,
		GoodsKeywords: rGoods.GoodsKeywords,
		GoodsDesc:     rGoods.GoodsDesc,
		GoodsContent:  rGoods.GoodsContent,
		IsDelete:      rGoods.IsDelete,
		IsHot:         rGoods.IsHot,
		IsBest:        rGoods.IsBest,
		IsNew:         rGoods.IsNew,
		GoodsTypeId:   rGoods.GoodsTypeId,
		Sort:          rGoods.Sort,
		Status:        rGoods.Status,
		AddTime:       utils.GetUnix(),
		GoodsColor:    goodsColorStr,
		GoodsImg:      goodsImg,
	}

	if err := global.MysqlDb.Create(&goods).Error; err != nil {
		return err
	}
	//增加图库 信息
	for _, v := range rGoods.GoodsImageList {
		goodsImgObj := models.GoodsImage{
			GoodsId: goods.Id,
			ImgUrl:  v,
			Sort:    10,
			Status:  models.Active,
			AddTime: utils.GetUnix(),
		}
		global.MysqlDb.Create(&goodsImgObj)
	}

	//增加规格包装
	for i := 0; i < len(rGoods.AttrIdList); i++ {
		var goodsTypeAttributeObj models.GoodsTypeAttribute
		if err := global.MysqlDb.First(&goodsTypeAttributeObj, rGoods.AttrIdList[i]).Error; err != nil {
			continue
		}
		goodsAttrObj := models.GoodsTypeAttr{}
		goodsAttrObj.GoodsId = goods.Id
		goodsAttrObj.AttributeTitle = goodsTypeAttributeObj.Title
		goodsAttrObj.AttributeType = goodsTypeAttributeObj.AttrType
		goodsAttrObj.AttributeId = goodsTypeAttributeObj.Id
		goodsAttrObj.AttributeCateId = goodsTypeAttributeObj.CateId
		goodsAttrObj.AttributeValue = rGoods.AttrValueList[i]
		goodsAttrObj.Status = models.Active
		goodsAttrObj.Sort = 10
		goodsAttrObj.AddTime = utils.GetUnix()
		global.MysqlDb.Create(&goodsAttrObj)
	}
	return nil
}

func GoodsEdie(id string) (*models.Goods, []models.GoodsCate, []models.GoodsColor, []models.GoodsImage, []models.GoodsType, string, error) {
	//获取商品信息
	var goods models.Goods
	if err := global.MysqlDb.First(&goods, id).Error; err != nil {
		return nil, nil, nil, nil, nil, "", err
	}
	//2、获取商品分类
	var goodsCate []models.GoodsCate
	global.MysqlDb.Where("pid=?", 0).Preload("GoodsCateItem").Find(&goodsCate)
	// 3、获取所有颜色 以及选中的颜色
	goodsColorSlice := strings.Split(goods.GoodsColor, ",")
	goodsColorMap := make(map[string]string)
	for _, v := range goodsColorSlice {
		goodsColorMap[v] = v
	}
	//获取颜色信息
	var goodsColor []models.GoodsColor
	global.MysqlDb.Find(&goodsColor)
	for i := 0; i < len(goodsColor); i++ {
		_, ok := goodsColorMap[strconv.Itoa(goodsColor[i].Id)]
		if ok {
			goodsColor[i].Checked = true
		}
	}

	//商品的图库信息
	var goodsImage []models.GoodsImage
	global.MysqlDb.Where("goods_id=?", goods.Id).Find(&goodsImage)

	// 5、获取商品类型
	var goodsType []models.GoodsType
	global.MysqlDb.Find(&goodsType)

	//6、获取规格信息
	var goodsAttr []models.GoodsTypeAttr
	global.MysqlDb.Where("goods_id=?", goods.Id).Find(&goodsAttr)

	var goodsAttrStr string
	for _, v := range goodsAttr {
		if v.AttributeType == 1 { //单行框
			goodsAttrStr += fmt.Sprintf(`<li><span>%v: 　</span>  <input type="hidden" name="attr_id_list" value="%v" />   <input type="text" name="attr_value_list" value="%v" /></li>`, v.AttributeTitle, v.AttributeId, v.AttributeValue)
		} else if v.AttributeType == 2 { //多行框
			goodsAttrStr += fmt.Sprintf(`<li><span>%v: 　</span><input type="hidden" name="attr_id_list" value="%v" />  <textarea cols="50" rows="3" name="attr_value_list">%v</textarea></li>`, v.AttributeTitle, v.AttributeId, v.AttributeValue)
		} else {
			// 获取 attr_value  获取可选值列表
			oneGoodsTypeAttribute := models.GoodsTypeAttribute{Id: v.AttributeId}
			global.MysqlDb.First(&oneGoodsTypeAttribute)
			attrValueSlice := strings.Split(oneGoodsTypeAttribute.AttrValue, "\n")
			goodsAttrStr += fmt.Sprintf(`<li><span>%v: 　</span>  <input type="hidden" name="attr_id_list" value="%v" /> `, v.AttributeTitle, v.AttributeId)
			goodsAttrStr += fmt.Sprintf(`<select name="attr_value_list">`)
			for j := 0; j < len(attrValueSlice); j++ {
				if attrValueSlice[j] == v.AttributeValue {
					goodsAttrStr += fmt.Sprintf(`<option value="%v" selected >%v</option>`, attrValueSlice[j], attrValueSlice[j])
				} else {
					goodsAttrStr += fmt.Sprintf(`<option value="%v">%v</option>`, attrValueSlice[j], attrValueSlice[j])
				}
			}
			goodsAttrStr += fmt.Sprintf(`</select>`)
			goodsAttrStr += fmt.Sprintf(`</li>`)
		}
	}
	return &goods, goodsCate, goodsColor, goodsImage, goodsType, goodsAttrStr, nil
}

func DoGoodsEdie(rGoods request.AdminGoodsRequest, c *gin.Context) error {
	//查出需要修改的用户
	if rGoods.Id == 0 {
		return errors.New("非法的请求")
	}
	//2、获取颜色信息 把颜色转化成字符串
	goodsColorStr := strings.Join(rGoods.GoodsColor, ",")

	var goods models.Goods
	global.MysqlDb.First(&goods, rGoods.Id)
	goods.Title = rGoods.Title
	goods.SubTitle = rGoods.SubTitle
	goods.GoodsSn = rGoods.GoodsSn
	goods.CateId = rGoods.CateId
	goods.GoodsNumber = rGoods.GoodsNumber
	goods.MarketPrice = rGoods.MarketPrice
	goods.Price = rGoods.Price
	goods.RelationGoods = rGoods.RelationGoods
	goods.GoodsAttr = rGoods.GoodsAttr
	goods.GoodsVersion = rGoods.GoodsVersion
	goods.GoodsGift = rGoods.GoodsGift
	goods.GoodsFitting = rGoods.GoodsFitting
	goods.GoodsKeywords = rGoods.GoodsKeywords
	goods.GoodsDesc = rGoods.GoodsDesc
	goods.GoodsContent = rGoods.GoodsContent
	goods.IsDelete = rGoods.IsDelete
	goods.IsHot = rGoods.IsHot
	goods.IsBest = rGoods.IsBest
	goods.IsNew = rGoods.IsNew
	goods.GoodsTypeId = rGoods.GoodsTypeId
	goods.Sort = rGoods.Sort
	goods.Status = rGoods.Status
	goods.GoodsColor = goodsColorStr

	//3、上传图片   生成缩略图
	goodsImg, err := UploadImg("goods_img", c)
	if err == nil && len(goodsImg) > 0 {
		goods.GoodsImg = goodsImg
	}

	//4、执行修改商品
	if err := global.MysqlDb.Save(&goods).Error; err != nil {
		return err
	}
	//修改图库数据 （增加）

	for _, v := range rGoods.GoodsImageList {
		goodsImgObj := models.GoodsImage{}
		goodsImgObj.GoodsId = goods.Id
		goodsImgObj.ImgUrl = v
		goodsImgObj.Sort = 10
		goodsImgObj.Status = 1
		goodsImgObj.AddTime = utils.GetUnix()
		global.MysqlDb.Create(&goodsImgObj)
	}

	//修改商品类型属性数据         1、删除当前商品id对应的类型属性  2、执行增加
	//删除当前商品id对应的类型属性
	var goodsAttrObj models.GoodsTypeAttr
	global.MysqlDb.Where("goods_id=?", goods.Id).Delete(&goodsAttrObj)
	//执行增加
	for i := 0; i < len(rGoods.AttrIdList); i++ {
		goodsTypeAttributeId, _ := strconv.Atoi(rGoods.AttrIdList[i])
		var goodsTypeAttributeObj models.GoodsTypeAttribute
		if err := global.MysqlDb.First(&goodsTypeAttributeObj, goodsTypeAttributeId).Error; err != nil {
			continue
		}

		goodsAttrObj := models.GoodsTypeAttr{}
		goodsAttrObj.GoodsId = goods.Id
		goodsAttrObj.AttributeTitle = goodsTypeAttributeObj.Title
		goodsAttrObj.AttributeType = goodsTypeAttributeObj.AttrType
		goodsAttrObj.AttributeId = goodsTypeAttributeObj.Id
		goodsAttrObj.AttributeCateId = goodsTypeAttributeObj.CateId
		goodsAttrObj.AttributeValue = rGoods.AttrValueList[i]
		goodsAttrObj.Status = 1
		goodsAttrObj.Sort = 10
		goodsAttrObj.AddTime = utils.GetUnix()
		global.MysqlDb.Create(&goodsAttrObj)
	}

	return nil
}

func GoodsDelete(id string) error {

	var goods models.Goods
	if err := global.MysqlDb.Where("id = ?", id).Delete(&goods).Error; err != nil {
		//删除属性
		var goodsAttr models.GoodsTypeAttr
		global.MysqlDb.Where("goods_id=?", id).Delete(&goodsAttr)
		//删除图库信息
		var goodsImage models.GoodsImage
		global.MysqlDb.Where("goods_id=?", id).Delete(&goodsImage)
	}
	return nil
}

func DoUpload(c *gin.Context) map[string]string {
	m := map[string]string{"link": ""}
	if savePath, err := UploadImg("file", c); err == nil {
		m["link"] = "/" + savePath
	}
	return m
}

//获取商品类型属性
func GetGoodsTypeAttribute(cateId string) map[string]interface{} {
	m := map[string]interface{}{
		"result":  "",
		"success": false,
	}
	var GoodsTypeAttribute []models.GoodsTypeAttribute
	if err := global.MysqlDb.Where("cate_id=?", cateId).Find(&GoodsTypeAttribute).Error; err == nil {
		m["result"] = GoodsTypeAttribute
		m["success"] = true
	}
	return m
}

//修改图片对应颜色信息
func ChangeGoodsImageColor(colorStrId, goodsImageId string) map[string]interface{} {
	m := map[string]interface{}{
		"result":  "跟新失败",
		"success": false,
	}
	colorId, err := strconv.Atoi(colorStrId)
	if err != nil {
		return m
	}
	var goodsImage models.GoodsImage
	if err := global.MysqlDb.First(&goodsImage, goodsImageId).Error; err != nil {
		return m
	}
	goodsImage.ColorId = colorId
	if err := global.MysqlDb.Save(&goodsImage).Error; err == nil {
		m["result"] = "更新成功"
		m["success"] = true
	}
	return m
}

//删除图库
func RemoveGoodsImage(goodsImageId string) map[string]interface{} {
	m := map[string]interface{}{
		"result":  "跟新失败",
		"success": false,
	}
	var goodsImage models.GoodsImage
	if err := global.MysqlDb.Where("id=?", goodsImageId).Delete(&goodsImage).Error; err == nil {
		m["result"] = "删除成功"
		m["success"] = true
	}

	return m
}
