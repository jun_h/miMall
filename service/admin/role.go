package admin

import (
	"errors"
	"strconv"
	"xiaomi/global"
	"xiaomi/models"
	"xiaomi/models/request"
	"xiaomi/utils"
)

//部门
func Role() ([]models.Role, error) {
	var roleList []models.Role
	if err := global.MysqlDb.Find(&roleList).Error; err != nil {
		return nil, err
	}
	return roleList, nil
}

func DoRoleAdd(rRole request.AdminRoleRequest) error {
	//判断数据库里面有没有当前部门
	count := 0
	if err := global.MysqlDb.Model(&models.Role{}).Where("title=?", rRole.Title).
		Count(&count).Error; err != nil || count > 0 {
		return errors.New("部门已经存在")
	}

	//创建部门模型
	role := models.Role{
		Title:       rRole.Title,
		Description: rRole.Description,
		Status:      models.Active,
		AddTime:     utils.GetUnix(),
	}

	if err := global.MysqlDb.Create(&role).Error; err != nil {
		return errors.New("增加管理员失败")
	}

	return nil
}

func RoleEdie(id string) (*models.Role, error) {
	var role models.Role
	if err := global.MysqlDb.First(&role, id).Error; err != nil {
		return nil, errors.New("部门不存在" + err.Error())
	}
	return &role, nil
}

func DoRoleEdie(rRole request.AdminRoleRequest) error {
	//查出需要修改的用户
	if rRole.Id == 0 {
		return errors.New("非法的请求")
	}
	var role models.Role
	if err := global.MysqlDb.First(&role, rRole.Id).Error; err != nil {
		return errors.New("部门不存在")
	}
	role.Title = rRole.Title
	role.Description = rRole.Description

	if err := global.MysqlDb.Save(&role).Error; err != nil {
		return errors.New("修改数据失败：" + err.Error())
	}

	return nil
}

func RoleDelete(id string) error {

	//判断数据库里面有没有当前用户
	count := 0
	if err := global.MysqlDb.Model(&models.Manager{}).Where("role_id=?", id).
		Count(&count).Error; err != nil || count > 0 {
		return errors.New("请先移除当前部门下所有管理员，在重新删除")
	}

	if err := global.MysqlDb.Where("id = ?", id).Delete(&models.Role{}).
		Error; err != nil {
		return errors.New("删除管理员失败：" + err.Error())
	}
	return nil
}

func RoleAuth(roleId string) ([]models.Access, error) {
	//1、获取全部的权限
	var accessList []models.Access
	if err := global.MysqlDb.Preload("AccessItem").Where("module_id=0").Find(&accessList).Error; err != nil {
		return nil, errors.New("系统错误：" + err.Error())
	}
	//2、获取当前角色拥有的权限 ，并把权限id放在一个map对象里面
	var roleAccess []models.RoleAccess
	if err := global.MysqlDb.Where("role_id=?", roleId).Find(&roleAccess).Error; err != nil {
		return nil, errors.New("系统错误：" + err.Error())
	}
	roleAccessMap := make(map[int]int)
	for _, v := range roleAccess {
		roleAccessMap[v.AccessId] = v.AccessId
	}
	//3、循环遍历所有的权限数据，判断当前权限的id是否在角色权限的Map对象中,如果是的话给当前数据加入checked属性(选中)
	for i := 0; i < len(accessList); i++ {
		if _, ok := roleAccessMap[accessList[i].Id]; ok {
			accessList[i].Checked = true
		}
		for j := 0; j < len(accessList[i].AccessItem); j++ {
			if _, ok := roleAccessMap[accessList[i].AccessItem[j].Id]; ok {
				accessList[i].AccessItem[j].Checked = true
			}
		}
	}

	return accessList, nil
}

func DoRoleAuth(roleStrId string, accessNode []string) error {
	if roleId, err := strconv.Atoi(roleStrId); err != nil {
		return errors.New("非法的请求：" + err.Error())
	} else {
		//1、修改角色权限---删除当前部门下面的所有权限
		roleAccess := models.RoleAccess{}
		if err := global.MysqlDb.Where("role_id=?", roleId).Delete(&roleAccess).Error; err != nil {
			return errors.New("系统故障：" + err.Error())
		}

		//2、判断传入的权限参数是否为空
		if len(accessNode) == 0 {
			return nil
		}

		//3、执行增加数据
		for _, v := range accessNode {
			accessId, _ := strconv.Atoi(v)
			roleAccess.AccessId = accessId
			roleAccess.RoleId = roleId
			global.MysqlDb.Create(&roleAccess)
		}
	}
	return nil
}
