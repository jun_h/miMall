package initialize

import (

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
	"xiaomi/global"
)

//var Db *gorm.DB

func Mysql() {

	admin := global.InitConfig.Mysql
	//fmt.Println(admin)
	if db, err := gorm.Open("mysql", admin.Username+":"+admin.Password+"@("+admin.Path+")/"+admin.Dbname+"?"+admin.Config); err != nil {
		panic("连接数据库不成功:" + err.Error())
	}else {
		//复制给全局db
		global.MysqlDb = db
		//设置db日志模式
		global.MysqlDb.LogMode(true)
		//设置连接池
		//空闲数
		global.MysqlDb.DB().SetMaxIdleConns(admin.MaxIdleConns)
		//最大连接数
		global.MysqlDb.DB().SetMaxOpenConns(admin.MaxOpenConns)
		//超时时间
		global.MysqlDb.DB().SetConnMaxLifetime(time.Second * 30)

		//自动创建表
		Migration(db)
	}
}
