package initialize

import (

	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"xiaomi/global"
)

const defaultConfigFile = "config.yaml"

func init() {
	v := viper.New()
	v.SetConfigFile(defaultConfigFile)
	if err := v.ReadInConfig(); err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	v.WatchConfig()

	v.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("config file changed:", e.Name)
		if err := v.Unmarshal(&global.InitConfig); err != nil {
			fmt.Println(err)
		}
	})

	//解析到结构体
	if err := v.Unmarshal(&global.InitConfig); err != nil {
		fmt.Println(err)
	}
	fmt.Printf("读取配置文件：%v",global.InitConfig)
}

func Init() {
	Mysql()
	//Redis()
}
