package initialize

import (

	"github.com/go-redis/redis"
	"xiaomi/global"
)

//Redis 在中间件中初始化redis链接
func Redis() {
	redisCfg := global.InitConfig.Redis
	client := redis.NewClient(&redis.Options{
		Addr:     redisCfg.Addr,
		Password: redisCfg.Password,
		DB:       redisCfg.DB,
	})

	if _, err := client.Ping().Result();err != nil {
		panic("连接Redis不成功")
	}else {
		global.RedisDb = client
	}
}
