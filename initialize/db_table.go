package initialize

import (

	"github.com/jinzhu/gorm"
	"xiaomi/models"
)

//执行数据迁移

func Migration(db *gorm.DB) {
	// 自动迁移模式
	db.AutoMigrate(&models.Manager{})
	db.AutoMigrate(&models.Role{})
	db.AutoMigrate(&models.Access{})
	db.AutoMigrate(&models.RoleAccess{})
	db.AutoMigrate(&models.Focus{})
	db.AutoMigrate(&models.Goods{})
	db.AutoMigrate(&models.GoodsCate{})
	db.AutoMigrate(&models.GoodsType{})
	db.AutoMigrate(&models.GoodsTypeAttr{})
	db.AutoMigrate(&models.GoodsTypeAttribute{})
	db.AutoMigrate(&models.GoodsColor{})
	db.AutoMigrate(&models.GoodsImage{})
}
